package reza.abbaszadeh.com.circular_progressbar

import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.util.TypedValue
import android.view.MotionEvent
import android.view.View
import androidx.annotation.Keep
import androidx.core.content.ContextCompat
import java.lang.Exception


const val SQUARE_SIZE_DEF: Int = 1000

class CircularSeekbar : View {

    private var startAngle = 135f
    private var sweepAngle = 270f
    private var point_radius = 20
    private lateinit var backPaint: Paint
    private lateinit var progPaint: Paint
    private lateinit var pointPaint: Paint

    private var strokeWidth = 3
    private var defStrokeWidth = 3
    private var progress = 0f
    private var padding = 0
    private var r = 0
    private lateinit var animator: ObjectAnimator
    private lateinit var onChanged: () -> Unit

    private var rectf = RectF()

    constructor(context: Context?) : super(context) {
        init(null)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(attrs)
    }


    fun init(set: AttributeSet?) {
        backPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        progPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        pointPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        backPaint.style = Paint.Style.STROKE
        progPaint.style = Paint.Style.STROKE


        val ta = context.obtainStyledAttributes(set, R.styleable.CircularSeekbar)
        defStrokeWidth = ta.getDimensionPixelSize(R.styleable.CircularSeekbar_strokeWidth, 8.toDp())
        strokeWidth = defStrokeWidth
        startAngle = ta.getFloat(R.styleable.CircularSeekbar_startAngle, 135f) % 360
        sweepAngle = ta.getFloat(R.styleable.CircularSeekbar_sweepAngle, 270f) % 361
        backPaint.strokeWidth = strokeWidth.toFloat()
        progPaint.strokeWidth = strokeWidth.toFloat()
        val backColor = ta.getColor(
            R.styleable.CircularSeekbar_backColor,
            ContextCompat.getColor(context, R.color.defBack)
        )
        val progColor = ta.getColor(
            R.styleable.CircularSeekbar_progressColor,
            ContextCompat.getColor(context, R.color.yellow)
        )
        val pointColor = ta.getColor(
            R.styleable.CircularSeekbar_pointColor,
            ContextCompat.getColor(context, R.color.defBack)
        )

        progress = ta.getFloat(R.styleable.CircularSeekbar_progress, 0f)

        backPaint.color = backColor
        progPaint.color = progColor
        pointPaint.color = pointColor
        backPaint.strokeWidth = strokeWidth.toFloat()
        progPaint.strokeWidth = strokeWidth.toFloat()

        ta.recycle()

    }

    inline fun Int.toDp() = TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        toFloat(),
        resources.displayMetrics
    ).toInt()


    override fun onDraw(canvas: Canvas?) {
        padding = point_radius + strokeWidth
        r = width / 2 - padding

        rectf.run {
            left = 0f + padding
            top = 0f + padding
            right = width.toFloat() - padding
            bottom = height.toFloat() - padding

        }

        canvas!!.drawArc(
            rectf,
            startAngle,
            sweepAngle,
            false,
            backPaint
        )
        canvas.drawArc(
            rectf,
            startAngle,
            progress * sweepAngle,
            false,
            progPaint
        )
        canvas.drawCircle(
            width / 2f + r * Math.cos(Math.toRadians(progress*sweepAngle.toDouble() + startAngle)).toFloat(),
            height / 2f + r * Math.sin(Math.toRadians(progress*sweepAngle.toDouble() + startAngle)).toFloat(),
            point_radius.toFloat(),
            pointPaint
        )
    }


    var touched = false
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        val x = event!!.x
        val y = event.y
        val cx = width / 2
        val cy = height / 2
        val dis_pow = Math.pow(x.toDouble() - cx, 2.0) + Math.pow(y.toDouble() - cy, 2.0)
        when (event.action) {
            MotionEvent.ACTION_MOVE -> {
                if (touched) {
                    try {
                        animator.cancel()
                    } catch (e: Exception) {
                    }
                    val dis = Math.sqrt(dis_pow)
                    setProgressXY((x - cx) / dis, (y - cy) / dis)
                    if (::onChanged.isInitialized) {
                        onChanged()
                    }
                }
            }

            MotionEvent.ACTION_DOWN -> {
                if (dis_pow in Math.pow(r - 20.0, 2.0)..Math.pow(r + 20.0, 2.0)) {
                    try {
                        animator.cancel()
                    } catch (e: Exception) {
                    }
                    touched = true
                    val dis = Math.sqrt(dis_pow)
                    setProgressXY((x - cx) / dis, (y - cy) / dis)
                    if (::onChanged.isInitialized) {
                        onChanged()
                    }
                }
                return true
            }

            MotionEvent.ACTION_UP -> {
                touched = false
            }


        }

        return super.onTouchEvent(event)
    }

    @Keep
    fun setProgress(value: Float) {
        if(value in 0f..1f) {
            progress = value
            invalidate()
        }
    }

    private fun setProgressXY(x: Double, y: Double) {
        var deg = Math.toDegrees(Math.acos(x)).toFloat()
        if (y > 0) {
            deg = 360 - deg
        }
        if (deg < 360 - startAngle) {
            deg = 360 - startAngle - deg
            setProgress(deg/sweepAngle)
        } else if (deg > 720 - startAngle - sweepAngle) {
            deg = 360 - (startAngle + deg) % 360
            setProgress(deg/sweepAngle)
        }
    }


    fun setOnProgressChangedListenerr(onChanged: () -> Unit) {
        this.onChanged = onChanged
    }

    fun AnimateProgress(duration: Long, progress: Float) {
        val from = this.progress*sweepAngle
        val to = progress * sweepAngle
        /*if (to < from) {
            val tmp = to
            to = from
            from = tmp
        }*/
        animator = ObjectAnimator.ofFloat(
            this,
            "progress",
            from,
            to
        )
            .apply {
                this.duration = duration
                start()
            }
    }


}